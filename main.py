from flask import Flask, request, jsonify

from model.post import Post

import json

posts = []

app = Flask(__name__)
        
class CustomJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Post):
            return obj.to_dict()
        return super().default(obj)
        

app.json_encoder = CustomJSONEncoder


def find_post_author(value, author_name='author'):
    for post in posts:
        if getattr(post, author_name) == value:
            return post
    return None

@app.route('/ping', methods=['GET'])
def ping():
    return jsonify({'responce': 'pong'})

@app.route('/post', methods=['POST'])
def create_post():
    '''{"body": "Text of post", "author": "@somebody"}
    '''
    post_json = request.get_json()
    post = Post(post_json['body'], post_json['author'])
    posts.append(post)
    return jsonify({'status': 'success'})

@app.route('/post', methods=['GET'])
def read_post():
    try:
        return jsonify({'posts': [post.to_dict() for post in posts]})
    except Exception as e:
        return str(e)
    
@app.route('/post', methods=['PUT'])
def update_post():
    post_json = request.get_json()
    author_value = post_json.get('author')
    post = find_post_author(author_value, author_name='author')
    if post:
        post.body = post_json.get('body', post.body)
        post.author = post_json.get('author', post.author)
        return jsonify({'status': 'updated'})
    else:
        return jsonify({'error': 'Post not found'}), 404

@app.route('/post', methods=['DELETE'])
def delete_post():
    post_json = request.get_json()
    author_value = post_json.get('author')
    post = find_post_author(author_value, author_name='author')
    if post:
        posts.remove(post)
        return jsonify({'status': 'deleted'})
    else:
        return jsonify({'error': 'Post not found'}), 404

if __name__ == '__main__':
    app.run(debug=True)